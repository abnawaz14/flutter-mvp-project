import 'package:flutter/cupertino.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';

class Utils {
  static double getSubTotal(var items) {
    double price = 0;
    for (Item item in items) {
      price += item.price;
    }
    return price;
  }

  static List<String> getKeys(var map) {
    List<String> mykeys = [];
    for (var key in map.keys) {
      mykeys.add(key);
    }
    return mykeys;
  }

  static List<String> getValues(var map) {
    List<String> myValues = [];
    for (var value in map.values) {
      myValues.add(value);
    }
    return myValues;
  }

  static getWidth(BuildContext context) => MediaQuery.of(context).size.width;
  static getHeight(BuildContext context) => MediaQuery.of(context).size.height;
}
