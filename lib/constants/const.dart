import 'package:flutter/material.dart';

var kDecoration = BoxDecoration(
  border: Border.all(
    width: 2.0,
    color: Colors.black12,
  ),
  borderRadius: BorderRadius.circular(8),
);
var kBoxDecoration = BoxDecoration(
  color: Colors.white,
  boxShadow: [
    BoxShadow(
      color: Colors.grey[200],
      blurRadius: 2,
      spreadRadius: 1,
    )
  ],
);
var kBoxDecoration2 = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(6),
  boxShadow: [
    BoxShadow(
      color: Colors.grey[200],
      blurRadius: 2,
      spreadRadius: 3,
    )
  ],
);
var kBoxDecoration3 = BoxDecoration(
  color: Colors.black,
  borderRadius: BorderRadius.circular(6),
  boxShadow: [
    BoxShadow(
      color: Colors.grey[200],
      blurRadius: 2,
      spreadRadius: 3,
    )
  ],
);
var circleShape = BoxDecoration(
  shape: BoxShape.circle,
  color: Colors.black,
);
var padding = const EdgeInsets.symmetric(
  horizontal: 10.0,
  vertical: 20.0,
);
var homeStyle =
    TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold);
var homeStyle2 = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

var gridStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.bold,
);

var textStyle = TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w900,
);

var textStyle2 = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w900,
);
var textStyle3 = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
);
