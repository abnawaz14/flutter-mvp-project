import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';

class ReusableTextField extends StatelessWidget {
  final String title;
  final String hint;
  final alignment;
  final width;

  ReusableTextField(
      {this.title,
      this.hint,
      this.alignment = MainAxisAlignment.center,
      this.width = double.infinity});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        mainAxisAlignment: alignment,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          Container(
            width: width,
            decoration: kBoxDecoration2,
            margin: EdgeInsets.only(top: 10),
            child: TextField(
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hint,
                hintStyle: TextStyle(
                  color: Colors.grey[300],
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
