import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/item_details_screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';

class GridViewWidget extends StatefulWidget {
  final String item;

  GridViewWidget(this.item);

  @override
  _GridViewWidgetState createState() => _GridViewWidgetState();
}

class _GridViewWidgetState extends State<GridViewWidget> {
  List<Item> myItems = [];
  @override
  Widget build(BuildContext context) {
    if (widget.item != "item") {
      myItems = Item.item.where((element) => element.isFavourite).toList();
    } else {
      myItems = Item.item;
    }
    return GridView.count(
      crossAxisCount: 2,
      mainAxisSpacing: 10,
      crossAxisSpacing: 25,
      childAspectRatio: 0.6,
      children: List.generate(
        myItems.length,
        (index) => GestureDetector(
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
            return ItemDetailScreen(myItems[index].index);
          })),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: Utils.getHeight(context) * 0.08,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(6.0),
                    child: Hero(
                      tag: "my${myItems[index].index}",
                      child: Image.asset(
                        myItems[index].path,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: Utils.getWidth(context) * 0.01,
                  bottom: Utils.getHeight(context) * 0.005,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      myItems[index].oldPrice > 0.0
                          ? Text(
                              "AED ${myItems[index].oldPrice.toStringAsFixed(2)}",
                              style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 10.0,
                                decoration: TextDecoration.lineThrough,
                                decorationColor: Colors.red,
                                decorationThickness: 3.0,
                              ),
                            )
                          : Container(),
                      Text(
                        "AED ${myItems[index].price.toStringAsFixed(2)}",
                        style: gridStyle,
                      ),
                      Text(
                        "${myItems[index].desc}",
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: Utils.getWidth(context) * 0.01,
                  left: Utils.getWidth(context) * 0.01,
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        Item.item[myItems[index].index].isFavourite =
                            !Item.item[myItems[index].index].isFavourite;
                      });
                    },
                    child: Icon(
                      myItems[index].isFavourite
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Colors.black,
                    ),
                  ),
                ),
                myItems[index].oldPrice > 0.0
                    ? Positioned(
                        top: 0,
                        right: Utils.getWidth(context) * 0.02,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 12.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.0),
                            color: Colors.black,
                          ),
                          child: Text(
                            "ON OFFER",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12.0,
                            ),
                          ),
                        ))
                    : Container(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
