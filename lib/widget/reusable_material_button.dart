import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';

class ReusableMaterialButton extends StatelessWidget {
  final String title;
  final Function pressMe;

  ReusableMaterialButton({this.title, this.pressMe});

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(minWidth: Utils.getWidth(context) / 1.1),
      child: MaterialButton(
        onPressed: pressMe,
        color: Colors.black,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        child: Text(
          title,
          style: textStyle2,
        ),
      ),
    );
  }
}
