import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';

class ReusableAppBar extends StatelessWidget {
  final String title;
  final Color color;
  final Widget body;
  final bool isPOP;

  ReusableAppBar(
      {this.title, this.body, this.color = Colors.white, this.isPOP = true});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: color,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            isPOP ? Navigator.pop(context) : print("ok");
          },
        ),
        title: Text(
          title,
          style: gridStyle.copyWith(
            fontSize: 16,
          ),
        ),
        centerTitle: true,
      ),
      body: body,
    );
  }
}
