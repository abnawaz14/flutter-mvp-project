import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';

class CustomAppBar extends StatelessWidget {
  final String title;
  final Widget body;

  CustomAppBar({this.title, this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          title,
          style: gridStyle,
        ),
        centerTitle: true,
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.all(Utils.getWidth(context) * 0.03),
            child: FlatButton(
              onPressed: () {},
              color: Colors.grey[200],
              child: Text(
                "CLEAR",
                style: textStyle,
              ),
            ),
          )
        ],
      ),
      body: body,
    );
  }
}
