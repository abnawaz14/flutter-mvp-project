import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/quantity_buttons.dart';

import 'baseline_widget.dart';

class ReusableBag extends StatelessWidget {
  final Item myItems;
  final bool showQuantity;

  ReusableBag({this.myItems, this.showQuantity = true});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      width: double.infinity,
      color: Colors.grey[200],
      child: Row(
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(Utils.getWidth(context) * 0.05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    myItems.title,
                    style: gridStyle.copyWith(
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "SIZE. M",
                    style: TextStyle(
                      color: Colors.black26,
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "COLOR:-",
                        style: TextStyle(
                          color: Colors.black26,
                          fontSize: 12,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                        width: 15,
                        height: 10,
                        decoration: BoxDecoration(
                          color: Colors.pinkAccent,
                          borderRadius: BorderRadius.circular(5),
                        ),
                      )
                    ],
                  ),
                  BaseLineWidget(
                    text: "AED",
                    price: myItems.price.round(),
                    identify: "bag",
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  showQuantity ? QuantityButton(myItems.quantity) : Container(),
                ],
              ),
            ),
          ),
          Container(
            width: Utils.getWidth(context) * 0.30,
            height: Utils.getHeight(context) * 0.25,
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 35),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(myItems.path), fit: BoxFit.fitHeight)),
          )
        ],
      ),
    );
  }
}
