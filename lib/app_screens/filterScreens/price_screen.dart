import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/custom_appbar.dart';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  double lowerValue = 0.0;
  double upperValue = 1500.0;
  RangeValues values = RangeValues(150.0, 1200.0);
  @override
  Widget build(BuildContext context) {
    return CustomAppBar(
      title: "PRICE",
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 20.0,
        ),
        child: Container(
          color: Colors.white,
          height: Utils.getHeight(context) * 0.20,
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "AED ${values.start.round()}",
                      style: textStyle.copyWith(
                        fontSize: 16,
                      ),
                    ),
                    Text(
                      "AED ${values.end.round()}",
                      style: textStyle.copyWith(
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                  activeTrackColor: Colors.black,
                  inactiveTrackColor: Colors.grey[200],
                  thumbColor: Colors.black,
                  overlayShape: SliderComponentShape.noThumb,
                  trackHeight: 5,
                ),
                child: RangeSlider(
                  max: upperValue,
                  min: lowerValue,
                  values: values,
                  onChanged: (RangeValues value) {
                    setState(() {
                      values = value;
                    });
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
