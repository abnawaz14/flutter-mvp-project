import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/filterScreens/brand_Screen.dart';
import 'package:flutter_mvp_company_project/app_screens/filterScreens/price_screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/filter_model.dart';
import 'package:flutter_mvp_company_project/widget/reusable_material_button.dart';

class ItemFilter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int i = 1;
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "FILTER",
          style: gridStyle,
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 20.0,
        ),
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(20),
              child: Column(
                children: List.generate(
                  FilterModel.list.length,
                  (index) => InkWell(
                    onTap: () {
                      switch (index) {
                        case 1:
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      BrandScreen()));
                          break;
                        case 3:
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      PriceScreen()));
                          break;
                      }
                    },
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                FilterModel.list[index].name,
                                style: gridStyle,
                              ),
                              index == 0
                                  ? StatefulBuilder(
                                      builder: (context,
                                          void Function(void Function())
                                              setState) {
                                        return GestureDetector(
                                          onTap: () {
                                            setState(() => i++);
                                          },
                                          child: Icon(
                                            FilterModel.list[index].value,
                                            color: i % 2 == 0
                                                ? Colors.black
                                                : Colors.grey[400],
                                          ),
                                        );
                                      },
                                    )
                                  : Text(
                                      FilterModel.list[index].value,
                                      style: TextStyle(
                                        color: Colors.grey[400],
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                            ],
                          ),
                          Divider(
                            thickness: 0.8,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Spacer(),
            ReusableMaterialButton(
              title: "VIEW ITEMS",
              pressMe: () {},
            ),
          ],
        ),
      ),
    );
  }
}
