import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/brand_model.dart';
import 'package:flutter_mvp_company_project/widget/custom_appbar.dart';

class BrandScreen extends StatefulWidget {
  @override
  _BrandScreenState createState() => _BrandScreenState();
}

class _BrandScreenState extends State<BrandScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomAppBar(
      title: "BRAND",
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 20.0,
        ),
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(20),
              child: Column(
                children: List.generate(
                  BrandModel.list.length,
                  (index) => InkWell(
                    onTap: () {
                      setState(() {
                        BrandModel.list[index].isChecked =
                            !BrandModel.list[index].isChecked;
                      });
                    },
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                BrandModel.list[index].name,
                                style: gridStyle,
                              ),
                              BrandModel.list[index].isChecked
                                  ? Icon(
                                      Icons.check,
                                      color: Colors.black,
                                    )
                                  : Container(),
                            ],
                          ),
                          Divider(
                            thickness: 0.8,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
