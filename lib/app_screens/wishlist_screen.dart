import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/widget/gridview_widget.dart';

class WishListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        title: Text(
          "WISHLIST",
          style: gridStyle,
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: padding,
        child: GridViewWidget("wishlist"),
      ),
    );
  }
}
