import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/baseline_widget.dart';
import 'package:flutter_mvp_company_project/widget/quantity_buttons.dart';

class ItemDetailScreen extends StatelessWidget {
  final int index;

  ItemDetailScreen(this.index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: Utils.getHeight(context) * 0.40,
              color: Colors.red,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Hero(
                    child: Image.asset(
                      '${Item.item[index].path}',
                      fit: BoxFit.cover,
                    ),
                    tag: "my$index",
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(Icons.arrow_back_ios, color: Colors.black),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Row(
                      children: <Widget>[
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                              Item.item[index].isFavourite
                                  ? Icons.favorite
                                  : Icons.favorite_border,
                              color: Colors.black),
                        ),
                        IconButton(
                          onPressed: () {},
                          icon: Icon(CupertinoIcons.collections,
                              color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: Utils.getWidth(context) / 2.5,
                    bottom: Utils.getWidth(context) * 0.05,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: 5,
                          height: 5,
                          decoration: circleShape,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                          width: 10,
                          height: 10,
                          decoration: circleShape,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                          width: 5,
                          height: 5,
                          decoration: circleShape,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        Item.item[index].title,
                        style: gridStyle.copyWith(
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            Item.item[index].desc,
                            style: TextStyle(
                              color: Colors.black38,
                            ),
                          ),
                          Item.item[index].oldPrice > 0.0
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      "AED ${Item.item[index].oldPrice.round()}",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14.0,
                                        decoration: TextDecoration.lineThrough,
                                        decorationColor: Colors.red,
                                        decorationThickness: 2.0,
                                      ),
                                    ),
                                    BaseLineWidget(
                                      text: "AED",
                                      price: Item.item[index].price.round(),
                                    ),
                                  ],
                                )
                              : Text(
                                  "AED ${Item.item[index].price.round()}",
                                  style: gridStyle.copyWith(
                                    fontSize: 20,
                                  ),
                                ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: Utils.getHeight(context) * 0.06,
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              decoration: kDecoration,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Select Size",
                                    style: gridStyle,
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down,
                                    color: Colors.black,
                                  )
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            width: Utils.getWidth(context) * 0.10,
                          ),
                          Expanded(
                            child: Container(
                              height: Utils.getHeight(context) * 0.06,
                              decoration: BoxDecoration(
                                color: Colors.black,
                                borderRadius: BorderRadius.circular(2),
                              ),
                              child: FlatButton.icon(
                                  onPressed: () {},
                                  icon: Icon(
                                    CupertinoIcons.time_solid,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    "SIZE GUIDE>>",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 12,
                                    ),
                                  )),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "SELECT COLOR",
                        style: textStyle.copyWith(
                          fontSize: 12,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          ReusableContainer(Colors.purple, false),
                          ReusableContainer(Colors.lightBlueAccent, false),
                          ReusableContainer(Colors.pinkAccent, true),
                        ],
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "QUANTITY",
                            style: gridStyle,
                          ),
                          QuantityButton(Item.item[index].quantity),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      ReusableRow(
                        title: "BRAND",
                        desc: "ZARA",
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      ReusableRow(
                        title: "IN STOCK",
                        desc: "4",
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Builder(
                            builder: (BuildContext context) {
                              return ReusableButton(
                                title: "ADD TO BAG",
                                icon: CupertinoIcons.collections,
                                pressMe: () {
                                  Item.item[index].addToBag = true;
                                  Scaffold.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        "item added to bag".toUpperCase(),
                                        style: textStyle3,
                                      ),
                                      backgroundColor: Colors.black87,
                                      action: SnackBarAction(
                                        onPressed: () {},
                                        label: "OK",
                                        textColor: Colors.white,
                                      ),
                                    ),
                                  );
                                },
                              );
                            },
                          ),
                          ReusableButton(
                            title: "CHECKOUT",
                            icon: CupertinoIcons.shopping_cart,
                            pressMe: () {},
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ReusableContainer extends StatelessWidget {
  final Color color;
  final bool selected;

  ReusableContainer(this.color, this.selected);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.20,
      height: MediaQuery.of(context).size.width * 0.1,
      margin: EdgeInsets.only(right: 15),
      child: Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 8, left: 8),
            decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10,
                    spreadRadius: 5,
                  )
                ]),
          ),
          selected
              ? Positioned(
                  top: 0,
                  left: 0,
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.green,
                    ),
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                      size: 15,
                    ),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}

class ReusableRow extends StatelessWidget {
  String title;
  String desc;

  ReusableRow({this.title, this.desc});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          title,
          style: gridStyle,
        ),
        Padding(
          padding: EdgeInsets.only(
            right: Utils.getWidth(context) * 0.1,
          ),
          child: Text(
            desc,
            style: gridStyle,
          ),
        )
      ],
    );
  }
}

class ReusableButton extends StatelessWidget {
  final title;
  final icon;
  final Function pressMe;
  ReusableButton({this.title, this.icon, this.pressMe});

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(minWidth: MediaQuery.of(context).size.width / 2.5),
      child: MaterialButton(
        onPressed: pressMe,
        color: Colors.black,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(
              icon,
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
