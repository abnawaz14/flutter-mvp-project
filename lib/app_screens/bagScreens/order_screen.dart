import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/bagScreens/place_order_Screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_appbar.dart';
import 'package:flutter_mvp_company_project/widget/reusable_material_button.dart';

class OrderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var myItem = Item.item.where((element) => element.addToBag).toList();
    var myMap = {
      "SUBTOTAL": "${Utils.getSubTotal(myItem)} AED",
      "DELIVERY": "25 AED",
      "COUPON CODE": "-130 AED",
    };
    TextEditingController myCoupon = TextEditingController();
    myCoupon.text = "MVP10";
    return ReusableAppBar(
      title: "ORDER SUMMARY",
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: padding,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Table(
                        children: [
                          TableRow(
                            children: [
                              Container(
                                color: Colors.black,
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        "ITEM(S)",
                                        style: textStyle3,
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        "QTY",
                                        style: textStyle3,
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        "PRICE",
                                        style: textStyle3,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          TableRow(
                            children: [
                              Column(
                                children: List.generate(
                                    myItem.length,
                                    (index) => ReusableTableRow(
                                          title: "${myItem[index].title}",
                                          quantity: "${myItem[index].quantity}",
                                          price:
                                              "${myItem[index].price.round()} AED",
                                        )),
                              )
                            ],
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                border: Border(
                                  bottom: BorderSide(color: Colors.grey[300]),
                                )),
                          ),
                          TableRow(
                            children: [
                              Column(
                                children: List.generate(
                                    3,
                                    (index) => ReusableTableRow(
                                          title: "",
                                          quantity:
                                              "${Utils.getKeys(myMap)[index]}",
                                          price:
                                              "${Utils.getValues(myMap)[index]}",
                                          index: index,
                                        )),
                              )
                            ],
                            decoration: BoxDecoration(
                                color: Colors.grey[100],
                                border: Border(
                                  bottom: BorderSide(color: Colors.grey[300]),
                                )),
                          ),
                          TableRow(
                            children: [
                              ReusableTableRow(
                                title: "",
                                quantity: "TOTAL",
                                price:
                                    "${Utils.getSubTotal(myItem).round() + 25 - 113} AED",
                              )
                            ],
                            decoration: BoxDecoration(
                              color: Colors.grey[100],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        color: Colors.black,
                        padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    color: Colors.white,
                                    height: Utils.getHeight(context) * 0.050,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0, vertical: 5),
                                      child: TextField(
                                        controller: myCoupon,
                                        style: textStyle,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: MaterialButton(
                                      color: Colors.white,
                                      onPressed: () {},
                                      child: Text(
                                        "APPLY",
                                        style: textStyle.copyWith(
                                          fontSize: 18,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: Text(
                                "MVP APPLICATION AND GAME DESIGN",
                                style: TextStyle(
                                  color: Colors.green.shade500,
                                  fontSize: 10,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: Utils.getHeight(context) * 0.04,
                      ),
                      Text(
                        "CHOOSE PAYMENT METHOD",
                        style: textStyle.copyWith(
                          fontSize: 14,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: Utils.getHeight(context) * 0.18,
                  color: Colors.grey[100],
                  child: Column(
                    children: <Widget>[
                      ReusableRow(
                        icon: Icon(
                          Icons.check_circle,
                          color: Colors.black,
                        ),
                        title: "CASH ON DELIVERY",
                      ),
                      ReusableRow(
                        icon: Icon(
                          Icons.radio_button_unchecked,
                          color: Colors.grey,
                        ),
                        title: "CREDIT / DEBIT CARD",
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          ReusableMaterialButton(
            title: "PLACE ORDER",
            pressMe: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => PlaceOrder())),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}

class ReusableTableRow extends StatelessWidget {
  final int index;
  final String title;
  final String quantity;
  final String price;

  ReusableTableRow({this.index = 0, this.title, this.quantity, this.price});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 10),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Text(
              title,
              style: textStyle.copyWith(
                fontSize: 12,
              ),
            ),
          ),
          Expanded(
            child: Text(
              quantity,
              style: textStyle.copyWith(
                fontSize: 12,
              ),
            ),
          ),
          Expanded(
            child: Text(
              price,
              style: TextStyle(
                color: index == 2 ? Colors.red.shade900 : Colors.black,
                fontWeight: FontWeight.w900,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ReusableRow extends StatelessWidget {
  final Icon icon;
  final String title;

  ReusableRow({this.icon, this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 22),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          icon,
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              title,
              style: textStyle,
            ),
          )
        ],
      ),
    );
  }
}
