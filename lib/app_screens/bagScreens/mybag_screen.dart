import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/bagScreens/shipping_address.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_bag.dart';
import 'package:flutter_mvp_company_project/widget/reusable_material_button.dart';

class MyBagScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Item> myItems = [];
    myItems = Item.item.where((element) => element.addToBag).toList();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        title: Text(
          "MY BAG",
          style: gridStyle,
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 20.0,
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemBuilder: (context, index) => ReusableBag(
                  myItems: myItems[index],
                ),
                itemCount: myItems.length,
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 8),
              width: double.infinity,
              color: Colors.grey[200],
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: Utils.getWidth(context) * 0.07,
                    vertical: Utils.getHeight(context) * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "SUBTOTAL",
                      style: gridStyle,
                    ),
                    Text(
                      "1500 AED",
                      style: gridStyle,
                    ),
                  ],
                ),
              ),
            ),
            ReusableMaterialButton(
              title: "CHECKOUT",
              pressMe: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ShippingDetails())),
            ),
          ],
        ),
      ),
    );
  }
}
