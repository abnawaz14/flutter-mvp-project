import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_appbar.dart';
import 'package:flutter_mvp_company_project/widget/reusable_material_button.dart';

class ShippingAddress extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReusableAppBar(
      title: "SELECT SHIPPING ADDRESS",
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 20.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: ListView(
                children: <Widget>[
                  ReusableColumn(
                    title: "ADDRESS LINE 1",
                    istextField: true,
                  ),
                  ReusableColumn(
                    title: "SELECT COUNTRY",
                    istextField: false,
                  ),
                  ReusableColumn(
                    title: "CITY / TOWN",
                    istextField: true,
                  ),
                  ReusableColumn(
                    title: "CONTACT NUMBER",
                    istextField: true,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "ADDRESS LABEL",
                        style: gridStyle,
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      ReusableContainer(
                        icon: Icons.home,
                        iconColor: Colors.black,
                        title: "HOME",
                        titleColor: Colors.black,
                        showDecoration: 2,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      ReusableContainer(
                        icon: Icons.work,
                        iconColor: Colors.white,
                        title: "WORK",
                        titleColor: Colors.white,
                        showDecoration: 3,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            ReusableMaterialButton(
              title: "SAVE ADDRESS",
              pressMe: () {},
            ),
          ],
        ),
      ),
    );
  }
}

class ReusableColumn extends StatelessWidget {
  final String title;
  final bool istextField;

  ReusableColumn({this.title, this.istextField});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 10,
        ),
        Text(
          title,
          style: textStyle,
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.06,
          decoration: kBoxDecoration2,
          child: istextField
              ? TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                  ),
                )
              : Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.black,
                  ),
                ),
        ),
      ],
    );
  }
}

class ReusableContainer extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final String title;
  final Color titleColor;
  final int showDecoration;

  ReusableContainer(
      {this.icon,
      this.iconColor,
      this.title,
      this.titleColor,
      this.showDecoration});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      height: Utils.getHeight(context) * 0.05,
      decoration: showDecoration == 2 ? kBoxDecoration2 : kBoxDecoration3,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Icon(
            icon,
            color: iconColor,
          ),
          SizedBox(
            width: 4,
          ),
          Text(
            title,
            style: TextStyle(
              color: titleColor,
              fontWeight: FontWeight.w900,
            ),
          )
        ],
      ),
    );
  }
}
