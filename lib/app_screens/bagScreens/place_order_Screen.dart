import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/profile_screens/order_Screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_back_button.dart';
import 'package:flutter_mvp_company_project/widget/reusable_material_button.dart';

class PlaceOrder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BackButtons(context),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'images/order.jpg',
            fit: BoxFit.cover,
            height: Utils.getHeight(context) * 0.45,
          ),
          Divider(
            thickness: 2,
            height: 1,
            indent: Utils.getWidth(context) * 0.06,
            endIndent: Utils.getWidth(context) * 0.06,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              "THANK YOU FOR YOU ORDER",
              style: gridStyle.copyWith(
                fontSize: 22,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: Utils.getWidth(context) * 0.15),
            child: Text(
              "YOUR ORDER HAS BEEN PLACED SUCCESSFULLY",
              style: gridStyle.copyWith(
                fontSize: 12,
              ),
            ),
          ),
          ReusableMaterialButton(
              title: "TRACK ORDER",
              pressMe: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => OrdersScreen()))),
        ],
      ),
    );
  }
}
