import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/profile_screens/track_order_screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';
import 'package:flutter_mvp_company_project/widget/reusable_appbar.dart';

class OrdersScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var list = Item.item.where((element) => element.addToBag).toList();
    return ReusableAppBar(
      title: "MY ORDERS",
      body: ListView(
        children: <Widget>[
          ReusableColumn(
            orderTitle: "ORDER#112244",
            orderPlaced: "Placed on: 29/07/2020",
            list: list,
            price: "AED 1195",
            confirmation: "WAITING FOR CONFIRMATION",
          ),
          ReusableColumn(
            orderTitle: "ORDER#112255",
            orderPlaced: "Placed on: 30/07/2020",
            list: list,
            price: "AED 2095",
            confirmation: "DELIVERED",
          )
        ],
      ),
    );
  }
}

class ReusableColumn extends StatelessWidget {
  final String orderTitle;
  final String orderPlaced;
  final list;
  final String price;
  final String confirmation;

  ReusableColumn(
      {this.orderTitle,
      this.orderPlaced,
      this.list,
      this.price,
      this.confirmation});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (BuildContext context) => TrackOrderScreen(
            orderTitle: orderTitle,
            price: price,
          ),
        ),
      ),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 30.0),
            child: Container(
              width: double.infinity,
              color: Colors.black,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        orderTitle,
                        style: textStyle3.copyWith(
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        orderPlaced,
                        style: textStyle3.copyWith(
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        "VIEW DETAILS>>",
                        style: textStyle3.copyWith(
                          fontSize: 12,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            width: double.infinity,
            color: Colors.grey[100],
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  children: List.generate(
                    list.length,
                    (index) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Text(
                        list[index].title,
                        style: TextStyle(
                          color: Colors.black26,
                          fontWeight: FontWeight.w900,
                          fontSize: 14,
                        ),
                      ),
                    ),
                  ),
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: <Widget>[
                    Text(
                      price,
                      style: textStyle.copyWith(
                        fontSize: 18,
                      ),
                    ),
                    Text(
                      confirmation,
                      style: TextStyle(
                        color: confirmation == "DELIVERED"
                            ? Colors.green
                            : Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
