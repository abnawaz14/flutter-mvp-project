import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_appbar.dart';
import 'package:flutter_mvp_company_project/widget/reusable_material_button.dart';
import 'package:flutter_mvp_company_project/widget/reusable_textfield.dart';

import 'credit_card_screen.dart';

class WalletScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReusableAppBar(
      title: "MY WALLET",
      color: Colors.black,
      body: Padding(
        padding: EdgeInsets.only(top: Utils.getHeight(context) * 0.08),
        child: Stack(
          fit: StackFit.expand,
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              color: Colors.white,
              height: double.infinity,
              margin: EdgeInsets.only(top: Utils.getHeight(context) * 0.16),
              child: ReusableTextField(
                title: "RECHARGE",
                hint: "RECHARGE AMOUNT",
              ),
            ),
            Positioned(
              top: 0,
              left: 5,
              right: 5,
              bottom: Utils.getHeight(context) * 0.45,
              child: Card(
                elevation: 15,
                shadowColor: Colors.black38,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12.0, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "MY WALLET",
                            style: textStyle.copyWith(
                              fontSize: 18,
                            ),
                          ),
                          Icon(
                            Icons.account_balance_wallet,
                            color: Colors.black,
                            size: 40,
                          )
                        ],
                      ),
                    ),
                    Text(
                      "TOTAL BALANCE",
                      style: gridStyle.copyWith(
                        fontSize: 12,
                      ),
                    ),
                    Text(
                      "AED 250",
                      style: textStyle.copyWith(
                        fontSize: 50,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "EVERY ",
                              style: TextStyle(
                                color: Colors.black26,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            TextSpan(
                              text: "AED 200 ",
                              style: textStyle,
                            ),
                            TextSpan(
                              text: "RECHARGE YOU GET ",
                              style: TextStyle(
                                color: Colors.black26,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            TextSpan(
                              text: "AED 25 ",
                              style: textStyle,
                            ),
                            TextSpan(
                              text: "FREE",
                              style: TextStyle(
                                color: Colors.black26,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * 0.03,
              child: ReusableMaterialButton(
                title: "RECHARGE",
                pressMe: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => CreditCardScreen())),
              ),
            )
          ],
        ),
      ),
    );
  }
}
