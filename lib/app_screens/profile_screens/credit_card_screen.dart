import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_appbar.dart';
import 'package:flutter_mvp_company_project/widget/reusable_material_button.dart';
import 'package:flutter_mvp_company_project/widget/reusable_textfield.dart';

class CreditCardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReusableAppBar(
      title: "ADD CREDIT CARD",
      body: Column(
        children: <Widget>[
          ReusableTextField(
            title: "CARD HOLDER NAME",
            hint: "ABU BAKAR NAWAZ",
            alignment: MainAxisAlignment.start,
          ),
          ReusableTextField(
            title: "CARD NUMBER",
            hint: "1234 567 890 1234",
            alignment: MainAxisAlignment.start,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              ReusableTextField(
                title: "EXPIRY DATE",
                hint: "YY/MM",
                alignment: MainAxisAlignment.start,
                width: Utils.getWidth(context) * 0.30,
              ),
              ReusableTextField(
                title: "CV",
                hint: "125",
                alignment: MainAxisAlignment.start,
                width: Utils.getWidth(context) * 0.30,
              ),
            ],
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(bottom: 15.0),
            child: ReusableMaterialButton(
              title: "CONTINUE",
              pressMe: () => Navigator.pop(context),
            ),
          )
        ],
      ),
    );
  }
}
