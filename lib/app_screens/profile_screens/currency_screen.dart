import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/currency_model.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_appbar.dart';

class CurrencyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReusableAppBar(
      title: "CURRENCY",
      color: Colors.grey[100],
      body: Container(
        margin: EdgeInsets.only(top: 20),
        color: Colors.white,
        height: Utils.getHeight(context) * 0.22,
        padding: EdgeInsets.all(20),
        child: Column(
          children: List.generate(
            CurrencyModel.list.length,
            (index) => Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      CurrencyModel.list[index].title,
                      style: textStyle,
                    ),
                    CurrencyModel.list[index].icon,
                  ],
                ),
                Divider(
                  thickness: 0.8,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
