import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/items_model.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_back_button.dart';
import 'package:flutter_mvp_company_project/widget/reusable_bag.dart';

class TrackOrderScreen extends StatelessWidget {
  final String orderTitle;
  final String price;

  TrackOrderScreen({this.orderTitle, this.price});

  @override
  Widget build(BuildContext context) {
    List<Item> myItems = [];
    myItems = Item.item.where((element) => element.addToBag).toList();
    return Scaffold(
      appBar: BackButtons(context),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text(
                orderTitle,
                style: textStyle.copyWith(
                  fontSize: 25,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text(
                price,
                style: textStyle.copyWith(
                  fontSize: 20,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Utils.getWidth(context) * 0.10,
                  vertical: Utils.getHeight(context) * 0.03),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        Icons.check_circle,
                        color: Colors.green,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0),
                        child: Icon(
                          Icons.library_books,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        "ORDER HAS BEEN PLACED",
                        style: gridStyle,
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: Utils.getWidth(context) * 0.03),
                    child: DotWidget(),
                  ),
                  ReusableWidget(
                    icon: Icons.card_giftcard,
                    text: "PREPARE YOUR ORDER",
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(left: Utils.getWidth(context) * 0.03),
                    child: DotWidget(),
                  ),
                  ReusableWidget(
                    icon: Icons.directions_car,
                    text: "ORDER FOR SHIPMENT",
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 25, bottom: 5),
              child: Text(
                "ORDER ITEMS",
                style: textStyle.copyWith(fontSize: 18),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: myItems.length,
                  itemBuilder: (context, index) => ReusableBag(
                        myItems: myItems[index],
                        showQuantity: false,
                      )),
            )
          ],
        ),
      ),
    );
  }
}

class DotWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: List.generate(
          8,
          (index) => Container(
                width: 4,
                height: 4,
                margin: EdgeInsets.symmetric(vertical: 5 / 2),
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  shape: BoxShape.circle,
                ),
              )),
    );
  }
}

class ReusableWidget extends StatelessWidget {
  final IconData icon;
  final String text;

  ReusableWidget({this.icon, this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
            width: 30,
            height: 30,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Colors.green.shade100,
              shape: BoxShape.circle,
            ),
            child: Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                color: Colors.green,
                shape: BoxShape.circle,
              ),
            )),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Icon(
            icon,
            color: Colors.black,
          ),
        ),
        Text(
          text,
          style: gridStyle,
        ),
      ],
    );
  }
}
