import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/profile_screens/wallet_screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/first_option_group.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:flutter_mvp_company_project/widget/reusable_appbar.dart';

import 'currency_screen.dart';
import 'order_Screen.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ReusableAppBar(
      title: "MY PROFILE",
      color: Colors.grey[100],
      isPOP: false,
      body: Column(
        children: <Widget>[
          Container(
            height: Utils.getHeight(context) * 0.30,
            child: Stack(
              fit: StackFit.expand,
              overflow: Overflow.visible,
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    bottom: Utils.getHeight(context) * 0.16,
                  ),
                  child: ColorFiltered(
                    colorFilter: ColorFilter.mode(
                      Colors.grey,
                      BlendMode.hardLight,
                    ),
                    child: Image.asset(
                      'images/images13.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: Utils.getWidth(context) * 0.24,
                        height: Utils.getHeight(context) * 0.15,
                        margin: EdgeInsets.only(
                            top: Utils.getHeight(context) * 0.05),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 10,
                              spreadRadius: 1,
                              offset: Offset(5, 5),
                              color: Colors.black12,
                            ),
                          ],
                          image: DecorationImage(
                              image: AssetImage("images/user.png"),
                              fit: BoxFit.cover),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Text(
                          "WELCOME,\nABU BAKAR",
                          style: textStyle.copyWith(
                            fontSize: 16,
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                ReusableContainer(
                  cIndex: 1,
                ),
                SizedBox(
                  height: 20,
                ),
                ReusableContainer(
                  cIndex: 2,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ReusableContainer extends StatelessWidget {
  final int cIndex;

  ReusableContainer({this.cIndex});

  @override
  Widget build(BuildContext context) {
    List<dynamic> list = [];
    cIndex == 1 ? list = FirstGroup.firstList : list = FirstGroup.secondList;
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(top: 20),
      child: Column(
        children: List.generate(
          list.length,
          (index) => Column(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  switch (list[index].title) {
                    case "MY ORDER":
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  OrdersScreen()));
                      break;
                    case "MY WALLET":
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  WalletScreen()));
                      break;
                    case "CHANGE LANGUAGE":
                      showAlertDialog(context);
                      break;
                    case "CURRENCY":
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  CurrencyScreen()));
                      break;
                  }
                },
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Icon(
                        list[index].leading,
                        color: Colors.black,
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Text(
                        list[index].title,
                        style: gridStyle,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Icon(
                        list[index].trailing,
                        color: Colors.black,
                        size: 15,
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 0.8,
              )
            ],
          ),
        ),
      ),
    );
  }

  void showAlertDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.32,
          child: Column(
            children: <Widget>[
              Container(
                width: double.infinity,
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 12),
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                  ),
                ),
                child: Text(
                  "SELECT LANGUAGE",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
              ),
              ReusablePadding(
                icon: Icons.radio_button_unchecked,
                iconColor: Colors.grey,
                title: "🇦🇪",
                padding: EdgeInsets.all(20),
                language: "العربية",
                fontSize: 30.0,
              ),
              ReusablePadding(
                icon: Icons.check_circle,
                iconColor: Colors.black,
                title: "🇺🇸",
                padding: EdgeInsets.symmetric(horizontal: 20),
                language: "ENGLISH",
                fontSize: 20.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ReusablePadding extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final String title;
  final padding;
  final language;
  final double fontSize;

  ReusablePadding(
      {this.icon,
      this.iconColor,
      this.title,
      this.padding,
      this.language,
      this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            color: iconColor,
            size: 30,
          ),
          Text(
            title,
            style: gridStyle.copyWith(
              fontSize: 30,
            ),
          ),
          Text(
            language,
            style: gridStyle.copyWith(
              fontSize: fontSize,
            ),
          )
        ],
      ),
    );
  }
}
