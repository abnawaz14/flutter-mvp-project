import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/categoryScreens/items_screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/model/category_model.dart';
import 'package:flutter_mvp_company_project/provider/click_handle_provider.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:provider/provider.dart';

List<String> values = [
  "NEW IN",
  "JEANS",
  "JACKETS",
  "SHIRTS",
  "TSHIRTS",
  "POLO SHIRTS",
  "SHORTS",
  "DRESSES"
];

class CategoryOptionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ClickHandler>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            provider.setDataProvider = -1;
          },
        ),
        title: Text(
          Categories.categoryList[provider.getIndexProvider].name,
          style: gridStyle,
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: Text(
                "SHOP BY PRODUCT",
                style: gridStyle.copyWith(
                  fontSize: 20.0,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                  itemBuilder: (context, index) => InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ItemScreen(values[index])));
                        },
                        child: Container(
                          height: Utils.getHeight(context) * 0.06,
                          margin: EdgeInsets.only(
                              left: Utils.getWidth(context) * 0.03),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    values[index],
                                    style: gridStyle,
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.grey[300],
                                    size: 15,
                                  ),
                                ],
                              ),
                              Divider(
                                thickness: 0.8,
                              )
                            ],
                          ),
                        ),
                      ),
                  itemCount: values.length),
            ),
          ],
        ),
      ),
    );
  }
}
