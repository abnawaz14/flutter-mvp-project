import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/model/category_model.dart';
import 'package:flutter_mvp_company_project/provider/click_handle_provider.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';
import 'package:provider/provider.dart';

class WomenScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: Categories.categoryList.length,
      itemBuilder: (context, index) => GestureDetector(
        onTap: () {
          Provider.of<ClickHandler>(context, listen: false).setDataProvider =
              index;
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          height: Utils.getHeight(context) * 0.18,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: index == 0 ? Colors.red.shade600 : Colors.grey[200],
          ),
          child: Row(
            children: <Widget>[
              Container(
                width: Utils.getWidth(context) * 0.35,
                padding: EdgeInsets.only(left: Utils.getWidth(context) * 0.03),
                child: Text(
                  Categories.categoryList[index].name,
                  style: TextStyle(
                    color: index == 0 ? Colors.white : Colors.black,
                    fontWeight: FontWeight.w900,
                    fontSize: 14,
                  ),
                ),
              ),
              Expanded(
                child: ClipPath(
                  clipper: MyCustomClipper(),
                  child: Image.asset(
                    Categories.categoryList[index].path,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    var path = Path();
    path.lineTo(size.width * 0.1, 0);

    var controllerPoint = Offset(0, size.height / 2);
    var endpoint = Offset(size.width * 0.10, size.height);
    path.quadraticBezierTo(
        controllerPoint.dx, controllerPoint.dy, endpoint.dx, endpoint.dy);

    path.lineTo(size.width, size.height);

    path.lineTo(size.width, 0);

    path.close(); // to close path last line
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
