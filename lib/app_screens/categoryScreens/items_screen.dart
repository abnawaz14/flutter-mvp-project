import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///H:/flutter_ui_challenges/flutter_mvp_company_project/lib/app_screens/filterScreens/item_filter_screen.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/widget/gridview_widget.dart';

class ItemScreen extends StatelessWidget {
  final String name;

  ItemScreen(this.name);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          name,
          style: gridStyle,
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.tune,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ItemFilter()));
            },
          ),
        ],
      ),
      body: Padding(
        padding: padding,
        child: GridViewWidget("item"),
      ),
    );
  }
}
