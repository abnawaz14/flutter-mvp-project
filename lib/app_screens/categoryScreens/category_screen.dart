import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvp_company_project/app_screens/categoryScreens/category_option_screen.dart';
import 'package:flutter_mvp_company_project/app_screens/categoryScreens/women_screen.dart';
import 'package:flutter_mvp_company_project/provider/click_handle_provider.dart';
import 'package:provider/provider.dart';

class CategoryScreen extends StatefulWidget {
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  int isSelected;
  List<String> logoWidgets;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isSelected = 0;
    logoWidgets = ["MEN", "WOMEN", "KIDS", "HOME"];
  }

  @override
  Widget build(BuildContext context) {
    return Provider.of<ClickHandler>(context, listen: true).getIndexProvider >=
            0
        ? CategoryOptionScreen()
        : Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 0.5,
              title: CupertinoTextField(
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                ),
                suffix: Icon(
                  Icons.search,
                  color: Colors.black26,
                  size: 35,
                ),
                placeholder: "SEARCH...",
                placeholderStyle: TextStyle(
                  color: Colors.black26,
                  fontSize: 16,
                ),
              ),
            ),
            body: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10.0,
                vertical: 10.0,
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    margin: EdgeInsets.only(
                      bottom: 10.0,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30.0),
                      color: Colors.grey[200],
                    ),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: choiceList()),
                  ),
                  Expanded(
                    child: ShowBody(isSelected),
                  )
                ],
              ),
            ),
          );
  }

  List<Widget> choiceList() {
    List<Widget> value = [];
    for (int index = 0; index < logoWidgets.length; index++) {
      value.add(Theme(
        data: Theme.of(context).copyWith(
            canvasColor:
                Colors.transparent), // to make background color transparent
        child: ChoiceChip(
          label: Text(logoWidgets[index]),
          selected: isSelected == index,
          selectedColor: Colors.black87,
          onSelected: (bool value) {
            setState(() {
              isSelected = index;
            });
          },
          labelStyle: TextStyle(
            color: isSelected == index ? Colors.white : Colors.black,
            fontWeight: FontWeight.bold,
          ),
          backgroundColor: Colors.transparent,
        ),
      ));
    }
    return value;
  }
}

class ShowBody extends StatelessWidget {
  final int index;

  ShowBody(this.index);

  @override
  Widget build(BuildContext context) {
    switch (index) {
      case 0:
        return Center(
          child: Text("MEN"),
        );
        break;
      case 1:
        return WomenScreen();
        break;
      case 2:
        return Center(
          child: Text("KIDS"),
        );
        break;
      case 3:
        return Center(
          child: Text("HOME"),
        );
        break;
      default:
        return null;
    }
  }
}
