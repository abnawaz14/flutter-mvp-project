import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter_mvp_company_project/constants/const.dart';
import 'package:flutter_mvp_company_project/utils/utilities.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(kToolbarHeight),
        child: Container(
          padding: EdgeInsets.only(top: Utils.getWidth(context) * 0.04),
          child: AppBar(
            backgroundColor: Colors.white,
            elevation: 0.5,
            centerTitle: true,
            title: Text(
              "DARK",
              style: TextStyle(
                fontFamily: 'Asset',
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                letterSpacing: 5.5,
              ),
            ),
            leading: Icon(
              Icons.sort,
              color: Colors.black,
              size: 35.0,
            ),
          ),
        ),
      ),
      body: HomePageBody(),
    );
  }
}

class HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: padding,
          child: Column(
            children: <Widget>[
              DottedBorder(
                borderType: BorderType.RRect,
                radius: Radius.circular(12),
                dashPattern: [8, 16],
                color: Colors.black,
                strokeWidth: 5,
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 5.0),
                        child: Text(
                          "USE COUPON CODE",
                          style: homeStyle,
                        ),
                      ),
                      DottedBorder(
                        dashPattern: [4, 6],
                        color: Colors.white,
                        strokeWidth: 2,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "MVP20",
                            style: homeStyle,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: Text(
                          "AND GET 20% OFF",
                          style: homeStyle,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                height: Utils.getHeight(context) * 0.35,
                child: Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.centerLeft,
                  children: <Widget>[
                    ClipRRect(
                      child: Image.asset(
                        'images/image1.jpg',
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    Positioned(
                      left: Utils.getWidth(context) * 0.05,
                      child: Text(
                        "SUMMER\nSALE",
                        style: textStyle3.copyWith(
                          fontSize: 40.0,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                height: Utils.getHeight(context) * 0.35,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Stack(
                        fit: StackFit.expand,
                        alignment: Alignment.bottomCenter,
                        children: <Widget>[
                          ClipRRect(
                            child: Image.asset(
                              'images/image2.jpg',
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.circular(6),
                          ),
                          Positioned(
                            bottom: Utils.getHeight(context) * 0.06,
                            left: Utils.getWidth(context) * 0.02,
                            right: Utils.getWidth(context) * 0.02,
                            child: Container(
                              color: Colors.white,
                              alignment: Alignment.center,
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              child: Text(
                                "NEW ARRIVALS",
                                style: homeStyle2,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    Expanded(
                      child: Stack(
                        fit: StackFit.expand,
                        alignment: Alignment.bottomRight,
                        children: <Widget>[
                          ClipRRect(
                            child: Image.asset(
                              'images/image3.jpg',
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.circular(6),
                          ),
                          Positioned(
                            bottom: Utils.getHeight(context) * 0.02,
                            right: Utils.getWidth(context) * 0.01,
                            child: Text(
                              "SHOP BY\nBRAND",
                              style: homeStyle2,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                height: Utils.getHeight(context) * 0.50,
                child: Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.centerLeft,
                  children: <Widget>[
                    ClipRRect(
                      child: Image.asset(
                        'images/images17.jpg',
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    Positioned(
                      left: MediaQuery.of(context).size.width * 0.05,
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Text(
                              "NEW DRESSES",
                              style: TextStyle(
                                color: Color(0xFF3770f6),
                                fontWeight: FontWeight.bold,
                                fontSize: 24.0,
                              ),
                            ),
                            color: Colors.white,
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            color: Colors.white,
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 2),
                            child: Text(
                              "SHOP NOW",
                              style: TextStyle(
                                color: Color(0xFF3770f6),
                                fontWeight: FontWeight.bold,
                                fontSize: 24.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10.0),
                height: Utils.getHeight(context) * 0.25,
                child: Stack(
                  fit: StackFit.expand,
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    ClipRRect(
                      child: Image.asset(
                        'images/shoes.jpg',
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    Positioned(
                      bottom: MediaQuery.of(context).size.height * 0.01,
                      child: Container(
                        color: Colors.white,
                        padding:
                            EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                        child: Text(
                          "NEW WOMEN BAGS",
                          style: homeStyle2,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
