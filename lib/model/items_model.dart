class Item {
  int index;
  String path;
  double price;
  double oldPrice;
  String desc;
  bool isFavourite;
  String title;
  bool addToBag;
  String quantity;

  Item(
      {this.path,
      this.price,
      this.desc,
      this.isFavourite,
      this.oldPrice,
      this.index,
      this.title,
      this.addToBag,
      this.quantity});

  static List<Item> item = [
    Item(
      path: 'images/images11.jpg',
      price: 250.00,
      desc: "Black Women Dress",
      isFavourite: false,
      oldPrice: 0.0,
      index: 0,
      title: "PINK DRESS",
      addToBag: false,
      quantity: "1",
    ),
    Item(
      path: 'images/images12.jpg',
      price: 350.00,
      desc: "Colorful Women Dress",
      isFavourite: true,
      oldPrice: 0.0,
      index: 1,
      title: "PINK DRESS",
      addToBag: false,
      quantity: "1",
    ),
    Item(
      path: 'images/image3.jpg',
      price: 500.00,
      desc: "Black Women Dress",
      isFavourite: true,
      oldPrice: 800.00,
      index: 2,
      title: "PINK DRESS",
      addToBag: true,
      quantity: "2",
    ),
    Item(
      path: 'images/images14.jpg',
      price: 300.00,
      desc: "Black Women Dress",
      isFavourite: false,
      oldPrice: 0.0,
      index: 3,
      title: "PINK DRESS",
      addToBag: true,
      quantity: "1",
    ),
    Item(
      path: 'images/images15.jpg',
      price: 350.00,
      desc: "Black Women Dress",
      isFavourite: false,
      oldPrice: 0.0,
      index: 4,
      title: "PINK DRESS",
      addToBag: false,
      quantity: "1",
    ),
    Item(
      path: 'images/images16.jpg',
      price: 250.00,
      desc: "Black Women Dress",
      isFavourite: false,
      oldPrice: 0.0,
      index: 5,
      title: "PINK DRESS",
      addToBag: false,
      quantity: "1",
    ),
    Item(
      path: 'images/images17.jpg',
      price: 250.00,
      desc: "Black Women Dress",
      isFavourite: false,
      oldPrice: 500.0,
      index: 6,
      title: "PINK DRESS",
      addToBag: false,
      quantity: "1",
    ),
  ];
}
