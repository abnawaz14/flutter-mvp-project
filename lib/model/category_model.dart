class Categories {
  String name;
  String path;

  Categories({this.name, this.path});

  static List<Categories> categoryList = [
    Categories(name: "ON SALE", path: "images/sales.jpg"),
    Categories(name: "DESIGNERS", path: "images/shoes.jpg"),
    Categories(name: "BRANDS", path: "images/image4.jpg"),
    Categories(name: "CLOTHING", path: "images/image3.jpg"),
    Categories(name: "SHOES", path: "images/shoes.jpg"),
    Categories(name: "ACCESSORIES", path: "images/image1.jpg"),
  ];
}
