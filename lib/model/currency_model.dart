import 'package:flutter/material.dart';

class CurrencyModel {
  String title;
  Icon icon;

  CurrencyModel({this.title, this.icon});

  static List<CurrencyModel> list = [
    CurrencyModel(
      title: "AED درهم",
      icon: Icon(
        Icons.check,
        color: Colors.black,
      ),
    ),
    CurrencyModel(
      title: "SAR سار",
      icon: Icon(
        Icons.check,
        color: Colors.white,
      ),
    ),
  ];
}
