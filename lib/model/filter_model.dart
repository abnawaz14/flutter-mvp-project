import 'package:flutter/material.dart';

class FilterModel {
  String name;
  dynamic value;

  FilterModel({this.name, this.value});

  static List<FilterModel> list = [
    FilterModel(name: "ON SALE", value: Icons.check),
    FilterModel(name: "BRAND", value: "BERSHKA. ZARA. PULL AND PEAR. TO..."),
    FilterModel(name: "SIZE", value: "S.M.XL"),
    FilterModel(name: "PRICE", value: "AED 150 - AED 1200"),
  ];
}
