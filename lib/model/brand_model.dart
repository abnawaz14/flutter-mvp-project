class BrandModel {
  String name;
  bool isChecked;

  BrandModel({this.name, this.isChecked});

  static List<BrandModel> list = [
    BrandModel(name: "BERSHKA", isChecked: true),
    BrandModel(name: "ZARA", isChecked: true),
    BrandModel(name: "PULL & BEAR", isChecked: true),
    BrandModel(name: "TOMMY HILFIGHTER", isChecked: true),
    BrandModel(name: "H&M", isChecked: false),
    BrandModel(name: "BOSS", isChecked: false),
    BrandModel(name: "CALVIN KLEIN", isChecked: false),
    BrandModel(name: "LEVI'S", isChecked: false),
    BrandModel(name: "DIESEL", isChecked: false),
  ];
}
