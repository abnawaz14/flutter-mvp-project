import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///H:/flutter_ui_challenges/flutter_mvp_company_project/lib/app_screens/categoryScreens/category_screen.dart';
import 'package:flutter_mvp_company_project/app_screens/home_Screen.dart';
import 'app_screens/profile_screens/profile_screen.dart';
import 'file:///H:/flutter_ui_challenges/flutter_mvp_company_project/lib/app_screens/bagScreens/mybag_screen.dart';
import 'package:flutter_mvp_company_project/app_screens/wishlist_screen.dart';
import 'package:flutter_mvp_company_project/custom_icons/my_flutter_app_icons.dart';
import 'package:flutter_mvp_company_project/custom_icons/my_hanger_icons.dart';
import 'package:flutter_mvp_company_project/provider/click_handle_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ClickHandler>(
      create: (BuildContext context) => ClickHandler(),
      lazy: false,
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          scaffoldBackgroundColor: Colors.white,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CupertinoTabBar(
        backgroundColor: Colors.white,
        onTap: (selectedIndex) {
          setState(() {
            index = selectedIndex;
          });
        },
        currentIndex: index,
        activeColor: Colors.black87,
        inactiveColor: Colors.black26,
        items: [
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.home),
            title: Text(
              'HOME',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.heart),
            title: Text(
              'WISHLIST',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(MyHanger.hanger),
            title: Text(
              'CATEGORIES',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.collections),
            title: Text(
              'MY BAG',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(MyFlutterApp.user),
            title: Text(
              'PROFILE',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
      body: ShowBody(index),
    );
  }
}

class ShowBody extends StatelessWidget {
  final int index;

  ShowBody(this.index);

  @override
  Widget build(BuildContext context) {
    switch (index) {
      case 0:
        return HomeScreen();
        break;
      case 1:
        return WishListScreen();
        break;
      case 2:
        return CategoryScreen();
        break;
      case 3:
        return MyBagScreen();
        break;
      case 4:
        return ProfileScreen();
        break;
      default:
        return null;
    }
  }
}
