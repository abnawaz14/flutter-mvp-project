import 'package:flutter/material.dart';

class ClickHandler with ChangeNotifier {
  int _index = -1;

  set setDataProvider(int index) {
    _index = index;
    notifyListeners();
  }

  get getIndexProvider => _index;
}
